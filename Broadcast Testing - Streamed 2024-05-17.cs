using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Text;
using System;

namespace BroadcastTesting
{
    partial class Program : MyGridProgram
    {
        #region Programmable Block Content

        
        // Name Of Broadcast Controller
        const string ReadBlock = "Broadcast Controller";
        // Name of LCD
        const string ChatLCD = "LCD Panel";
        // Name of Action Relay Block
        const string ActionBlock = "Action Relay";
        // Last Sender Name
        string LastSender = null;
        // Last Sent Message
        string LastMessage = null;
        // Send Message Flag
        bool SendMessage = false;
        // Is First Message Flag (Skips sending on first run)
        bool IsFirstMessage = true;

        public Program()
        {
            // Run Main() ever 100 game ticks
            Runtime.UpdateFrequency = UpdateFrequency.Update100;
        }


        public void Main(string argument, UpdateType updateSource)
        {
            // Print Last Info so we know script is working
            Echo("Running");
            Echo("Last Sender: " + LastSender);
            Echo("Last Message: " + LastMessage);
            // Get The Broadcast Controller Block
            IMyBroadcastController mBC = GridTerminalSystem.GetBlockWithName(ReadBlock) as IMyBroadcastController;
            // Get LCD Block
            IMyTextPanel Text = GridTerminalSystem.GetBlockWithName(ChatLCD) as IMyTextPanel;
            // Get Action Relay Block
            IMyTransponder ActionRelay = GridTerminalSystem.GetBlockWithName(ActionBlock) as IMyTransponder;
            // Ensure all blocks are present
            if (mBC != null && Text != null && ActionRelay != null) {
                // Check if SendMessage flag is true
                if (SendMessage)
                {
                    // Check if the IsFirstMessage Flag is false (i.e. Not First Message)
                    if (!IsFirstMessage)
                    {
                        // Transmit Message in Message 1 (Message0)
                        mBC.ApplyAction("Transmit Message 1");
                    } else
                    {
                        // Set IsFirstMessage to false to enable transmitting messages
                        IsFirstMessage = false;
                    }
                    // Reset SendMessage flag
                    SendMessage = false;
                }
                // Get Last Message from LCD
                StringBuilder TextContent = new StringBuilder("");
                // Read from LCD
                Text.ReadText(TextContent);
                // Convert LCD Text to String
                string ActualTextContent = TextContent.ToString();
                // Try Parse the String, Regex would be better, but Space Engineers doesn't allow Regex
                string[] FirstMessage = ActualTextContent.Split(new string[] { "\n\n" }, StringSplitOptions.None);
                // Ensure at least something was found
                if (FirstMessage.Length != 0) {
                    // Get the first match (most recent message)
                    string TheFirstMessage = FirstMessage[0];
                    // Try Get The Name Of The Sender by Splitting String with New Line
                    string[] MessageParts = TheFirstMessage.Split(new string[] { "\n" }, StringSplitOptions.None);
                    // Ensure at least 2 lines present
                    if (MessageParts.Length >= 2)
                    {
                        // Extract Name from Text and remove first character (which is an @)
                        string MessageSender = MessageParts[0].Substring(1);
                        // Build the Message itself
                        string Message = "";
                        // Set Flag for first Line (MessageSender)
                        bool First = true;
                        // Loop over lines
                        foreach (string Val in MessageParts)
                        {
                            // Ensure not first line
                            if (First) { First = false;  continue; }
                            // Check if Message content isn't blank
                            if (Message != "")
                            {
                                // Append a space (in case of multi line message)
                                Message += " ";
                            }
                            // Append Text to Message
                            Message += Val;
                        }
                        // Check the last message doesn't match & wasn't from the same Sender
                        if (MessageSender != LastSender || Message != LastMessage)
                        {
                            // Update LastSender with New Sender
                            LastSender = MessageSender;
                            // Update LastMessage with New Message
                            LastMessage = Message;
                            // Check if this is an !action Command
                            bool IsAction = false;
                            // Ensure length of string is at least 7 characters
                            if (Message.Length >= 7)
                            {
                                // Check if start of string is !action
                                if (Message.Substring(0 ,7) == "!action")
                                {
                                    // Set IsAction flag to true
                                    IsAction = true;
                                }
                            }
                            // Check if IsAction flag is true
                            if (IsAction)
                            {
                                // Ensure Message is more then 8 characters
                                if (Message.Length > 8)
                                {
                                    // Extract Rest of Message string & Split by Spaces
                                    string[] command = Message.Substring(8).Trim().Split(' ');
                                    // Ensure there is at least one part of the command available
                                    if (command.Length >= 1)
                                    {
                                        // Extract the first part of the command '!action <firstpart>'
                                        string Channel = command[0];
                                        // Create Int Reference to Parse to
                                        int ChannelInt;
                                        // Try Parse the provided value to an Int to ensure it is a number
                                        if (int.TryParse(Channel, out ChannelInt))
                                        {
                                            // Update ActionRelay Channel to provided Channel Number
                                            ActionRelay.Channel = ChannelInt;
                                            // Trigger SendSignal on ActionRelay
                                            ActionRelay.SendSignal();
                                            // Set up Broadcast Controller From Name to MessageSender
                                            mBC.SetValue("CustomName", new StringBuilder(MessageSender));
                                            // Update Message0 to Send Feedback
                                            mBC.SetValue("Message0", new StringBuilder("Triggered Action Channel: " + ChannelInt.ToString()));
                                            // Set SendMessage flag to trigger next tick, running Action Trigger after this didn't work properly during testing
                                            SendMessage = true;
                                        }
                                    }
                                }
                            }
                            else // Not an !action command
                            {
                                // Set up Broadcast Controller From Name to MessageSender
                                mBC.SetValue("CustomName", new StringBuilder(MessageSender));
                                // Update Message0 with Message
                                mBC.SetValue("Message0", new StringBuilder(Message));
                                // Set SendMessage flag to trigger next tick, running Action Trigger after this didn't work properly during testing
                                SendMessage = true;
                            }
                        } else
                        {
                            // Display Feedback on PB Terminal
                            Echo("No New Message To Send");
                        }
                    } else
                    {
                        // Display Feedback on PB Terminal
                        Echo("Message Content Not Found?");
                    }
                } else
                {
                    // Display Feedback on PB Terminal
                    Echo("Unable to Read Messages");
                }
            }
            else
            {
                // Display Feedback on PB Terminal
                Echo("Required Blocks Not Found");
            }
        }
        #endregion
    }
}